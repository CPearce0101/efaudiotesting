﻿//UPDATE
//\ RUNNING INTO EDITOR ERRORS ON UPDATING

namespace Audio
{
    using UnityEngine;
    using UnityEngine.Events;

    public class AudioAction : MonoBehaviour
    { 
        [HideInInspector] public AudioSource Source;

        [HideInInspector] public float LoopDelay;

        [HideInInspector] public UnityEvent ItemEvent;
        [HideInInspector] public UnityEvent TriggerEventEnter;
        [HideInInspector] public UnityEvent TriggerEventStay;
        [HideInInspector] public UnityEvent TriggerEventExit;
        [HideInInspector] public UnityEvent CollisionEventEnter;
        [HideInInspector] public UnityEvent CollisionEventStay;
        [HideInInspector] public UnityEvent CollisionEventExit;
        [HideInInspector] public UnityEvent AmbientEvent;

        [HideInInspector] public bool IsSingle;
        [HideInInspector] public bool IsMultiple;
        [HideInInspector] public bool ShowExtras;
        [HideInInspector] public bool HasEvents;
        [HideInInspector] public bool IsLoop;
        [HideInInspector] public bool IsCustomLoop;
        [HideInInspector] public bool IsMixed;
        [HideInInspector] public bool IsMixedLooping;
        [HideInInspector] public bool IsPlayedOnStart;
        [HideInInspector] public bool IsSingleClipEvent;
        //[HideInInspector] public bool IsitemMultiClipEvent;
        //[HideInInspector] public bool IsItemEvent;
        [HideInInspector] public bool IsTriggerEventEnter;
        [HideInInspector] public bool IsTriggerEventStay;
        [HideInInspector] public bool IsTriggerEventExit;
        [HideInInspector] public bool IsCollisionEventEnter;
        [HideInInspector] public bool IsCollisionEventStay;
        [HideInInspector] public bool IsCollisionEventExit;
        [HideInInspector] public bool IsAmbientEvent;

        private void Awake()
        {
            if (Source == null)
            {
                var sourcePrefab = Resources.Load<AudioSource>("Audio/AudioSource");
                Source = sourcePrefab;
            }
        }

        private void Start()
        {
            if (IsAmbientEvent)
                OnAmbientEvent();

            if (IsPlayedOnStart)
                Play();

        }

        public virtual void Play() { }

        //Update this and below out of class
        public void OnSFXEvent()
        {
            ItemEvent.Invoke();
        }

        public void OnAmbientEvent()
        {
            AmbientEvent.Invoke();
        }

        public void OnTriggerEvent()
        {
            TriggerEventEnter.Invoke();
        }

        public void OnCollisionEvent()
        {
            CollisionEventEnter.Invoke();
        }

        public void OnTriggerEnter(Collider other)
        {
            TriggerEventEnter.Invoke();
        }

        public void OnTriggerExit(Collider other)
        {
            TriggerEventEnter.Invoke();
        }

        //WILL RAPPIDLY REPEAT UNTIL LEAVE ONTRIGGERSTAY
        public void OnTriggerStay(Collider other)
        {
            TriggerEventEnter.Invoke();
        }

        public void OnCollisionEnter(Collision collision)
        {
            CollisionEventEnter.Invoke();
        }

        //WILL RAPPIDLY REPEAT UNTIL LEAVE ONTRIGGERSTAY
        public void OnCollisionStay(Collision collision)
        {
            CollisionEventStay.Invoke();
        }

        public void OnCollisionExit(Collision collision)
        {
            CollisionEventExit.Invoke();
        }

    }
}