﻿//Editor is erroring when using through AudioAction classes


namespace Audio
{
    using UnityEngine;

    [ExecuteInEditMode]
    public class AudioOptions
    {
        public bool IsPlayedOnStart;
        public bool ShowExtras;
        public bool IsLooping;
        public bool HasEvents;
        public bool IsAmbientEvent;
        public bool IsTriggerEventEnter;
        public bool IsTriggerEventStay;
        public bool IsTriggerEventExit;
        public bool IsCollisionEventEnter;
        public bool IsCollisionEventStay;
        public bool IsCollisionEventExit;
    }
}