﻿namespace Audio.CustomEditor
{
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(AudioAction), true)]
    [CanEditMultipleObjects]
    public class AudioActionEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            AudioAction action = target as AudioAction;

            DrawDefaultInspector();

            EditorGUILayout.Space();
            action.IsPlayedOnStart = GUILayout.Toggle(action.IsPlayedOnStart, "Play on Start");
            EditorGUILayout.Space();
            action.ShowExtras = GUILayout.Toggle(action.ShowExtras, "Options");
                EditorGUILayout.Space();

                if (action.ShowExtras)
                {
                    action.IsMixed = GUILayout.Toggle(action.IsMixed, "Use Procedural Version");
                    action.IsLoop = GUILayout.Toggle(action.IsLoop, "Loop");
                    action.IsCustomLoop = GUILayout.Toggle(action.IsCustomLoop, "Custom Loop");
                    EditorGUILayout.Space();
                if (action.IsCustomLoop)
                {
                    action.IsLoop = false;
                    EditorGUILayout.LabelField("Delay Time");
                    EditorGUILayout.FloatField(action.LoopDelay);
                }
                    EditorGUILayout.Space();
                    EditorGUILayout.Space();
                    action.HasEvents = GUILayout.Toggle(action.HasEvents, "Show Audio Events");
                    EditorGUILayout.Space();

                if (action.HasEvents)
                    {
                    // GUILayout.Label("Use for Music, Ambient sounds, and background loops");
                        action.IsAmbientEvent = GUILayout.Toggle(action.IsAmbientEvent, "Ambient Event");
                        EditorGUILayout.Space();

                        action.IsTriggerEventEnter = GUILayout.Toggle(action.IsTriggerEventEnter, "Trigger Event Enter");
                        action.IsTriggerEventStay = GUILayout.Toggle(action.IsTriggerEventStay, "Trigger Event Stay");
                        action.IsTriggerEventExit = GUILayout.Toggle(action.IsTriggerEventExit, "Trigger Event Exit");
                        EditorGUILayout.Space();

                        action.IsCollisionEventEnter = GUILayout.Toggle(action.IsCollisionEventEnter, "Collision Event Enter");
                        action.IsCollisionEventStay = GUILayout.Toggle(action.IsCollisionEventStay, "Collision Event Stay");
                        action.IsCollisionEventExit = GUILayout.Toggle(action.IsCollisionEventExit, "Collision Event Exit");

                    //EditorGUILayout.Space();
                    //action.IsItemEvent = GUILayout.Toggle(action.IsItemEvent, "Item Event");
                }
                }
                        
                EditorGUILayout.Space();                      

            //if (action.IsItemEvent)
            //{
            //    EditorGUILayout.Space();
            //    //show event
            //    var clipEvent = serializedObject.FindProperty("SFXEvent");
            //    //draw prop
            //    EditorGUILayout.PropertyField(clipEvent, true);
            //    serializedObject.ApplyModifiedProperties();
            //}

            if (action.IsTriggerEventEnter)
            {
                EditorGUILayout.Space();
                var clipEvent = serializedObject.FindProperty("TriggerEventEnter");
                EditorGUILayout.PropertyField(clipEvent, true);
                serializedObject.ApplyModifiedProperties();
            }

            if (action.IsTriggerEventStay)
            {
                EditorGUILayout.Space();
                var clipEvent = serializedObject.FindProperty("TriggerEventStay");
                EditorGUILayout.PropertyField(clipEvent, true);
                serializedObject.ApplyModifiedProperties();
            }

            if (action.IsTriggerEventExit)
            {
                EditorGUILayout.Space();
                var clipEvent = serializedObject.FindProperty("TriggerEventExit");
                EditorGUILayout.PropertyField(clipEvent, true);
                serializedObject.ApplyModifiedProperties();
            }

            if (action.IsCollisionEventEnter)
            {
                EditorGUILayout.Space();
                var clipEvent = serializedObject.FindProperty("CollisionEventEnter");
                EditorGUILayout.PropertyField(clipEvent, true);
                serializedObject.ApplyModifiedProperties();
            }

            if (action.IsCollisionEventStay)
            {
                EditorGUILayout.Space();
                var clipEvent = serializedObject.FindProperty("CollisionEventStay");
                EditorGUILayout.PropertyField(clipEvent, true);
                serializedObject.ApplyModifiedProperties();
            }

            if (action.IsCollisionEventExit)
            {
                EditorGUILayout.Space();
                var clipEvent = serializedObject.FindProperty("CollisionEventExit");
                EditorGUILayout.PropertyField(clipEvent, true);
                serializedObject.ApplyModifiedProperties();
            }

            if (action.IsAmbientEvent)
            {
                EditorGUILayout.Space();
                var clipEvent = serializedObject.FindProperty("AmbientEvent");
                EditorGUILayout.PropertyField(clipEvent, true);
                serializedObject.ApplyModifiedProperties();
            }

        }

    }
}