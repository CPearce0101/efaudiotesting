﻿// Extended From Richard Fine's Unity Scriptable Object Talk: https://youtu.be/6vmRwLYWNRo
// https://bitbucket.org/richardfine/scriptableobjectdemo/src/default/Assets/ScriptableObject/Audio/Editor/AudioEventEditor.cs

namespace Audio.CustomEditor
{
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(AudioEvent), true)]
    public class AudioEventEditor : Editor
    {
        [SerializeField] private AudioSource previewer;

        string bugInfo = "\n \n \n Known Preview Issues: \n \n \n" +
            "Issue 1 \n \n To play an non-mixed sound you must First \n toggle Mix " +
            "then press preview, \n retoggle Mix and standard clips will \n" +
            " now play on preview press. \n \n " +
            "Issue 2 \n \n"+
            " When turning looping off uncheck Loop \n toggle " +
            " & press preview again.\n Looping will stop. \n \n"
            +"\n \n These are because there is no update \n loop within the editor so nothing can \n" +
            " catch the toggle change \n Issue is being researched further";

        private void OnEnable()
        {
            previewer = EditorUtility.CreateGameObjectWithHideFlags
                ("Audio preview", HideFlags.HideAndDontSave, typeof(AudioSource)).GetComponent<AudioSource>();
        }

        private void OnDisable()
        {
            DestroyImmediate(previewer.gameObject);
        }

        public override void OnInspectorGUI()
        {
            AudioClipVariable audioClip = target as AudioClipVariable;

            GUILayout.Label("Audio Clip");
            audioClip.Value = (AudioClip)EditorGUILayout.ObjectField(audioClip.Value, typeof(AudioClip), true);

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            GUILayout.Label("Clip Notes: ");
            audioClip.DeveloperDescription = GUILayout.TextArea(audioClip.DeveloperDescription);

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            audioClip.IsLooping = GUILayout.Toggle(audioClip.IsLooping, "Loop");

            audioClip.IsMixed = GUILayout.Toggle(audioClip.IsMixed, "Mix");

            EditorGUILayout.Space();

            if (audioClip.IsMixed)
            {
                DrawDefaultInspector();
            }

            EditorGUILayout.Space();

            EditorGUI.BeginDisabledGroup(serializedObject.isEditingMultipleObjects);
            EditorGUILayout.Space();
            if (GUILayout.Button("Preview"))
            {
                if (audioClip.IsLooping)
                {
                    previewer.loop = true;                   
                }

                else if (!audioClip.IsLooping)
                {
                    previewer.loop = false;
                }

                audioClip.Play(previewer);
            }     
            EditorGUI.EndDisabledGroup();

            GUILayout.Label(bugInfo);
        }
    }
}