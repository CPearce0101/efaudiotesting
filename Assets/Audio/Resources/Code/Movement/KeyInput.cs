﻿//NOT FOR PRODUCTION
//For testing only


namespace Audio
{
    using UnityEngine;
    using UnityEngine.Events;

    public class KeyInput : MonoBehaviour
    {
        public float Speed;
        
        void Update()
        {
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            {
                transform.position += Vector3.forward * Speed * Time.deltaTime;
            }

            if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
            {
                transform.position += Vector3.back * Speed * Time.deltaTime;
            }

            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                transform.position += Vector3.left * Speed * Time.deltaTime;
            }

            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                transform.position += Vector3.right * Speed * Time.deltaTime;
            }
        }

        
       
    }

}
