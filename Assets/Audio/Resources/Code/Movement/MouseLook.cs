﻿//FOR TESTING ONLY
//NOT FOR PRODUCTION

namespace Audio
{
    using UnityEngine;
    using UnityEngine.Events;

    public class MouseLook : MonoBehaviour
    {
        private float _x;
        private float _y;

        public float Speed;
        
        void Update()
        {
            if (Input.GetMouseButton(0))
            {
                transform.Rotate(new Vector3(-Input.GetAxis("Mouse Y") * Speed, Input.GetAxis("Mouse X") * Speed, 0));
                _x = transform.rotation.eulerAngles.x;
                _y = transform.rotation.eulerAngles.y;
                transform.rotation = Quaternion.Euler(_x, _y, 0);
            }
        }
    }
}