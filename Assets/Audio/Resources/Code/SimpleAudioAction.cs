﻿/// <summary>
/// ARE THEY UNABLE DO TO EDITOR DEPENDENCY FOR DESIGNERS?
/// UPDATE - REFACTOR CLASSES FOR 1 RESP???
/// </summary>

namespace Audio
{
    using System.Collections;
    using UnityEngine;
    using UnityEngine.Events;

    public class SimpleAudioAction : MonoBehaviour
    {
        protected AudioSource Source;

        public bool IsPlayedOnStart;
        public bool HasStartDelay;        
        public float MinStartDelay;
        public float MaxStartDelay;

        public bool IsLoop;
        public bool IsCustomLooping;
        public float MinLoopDelay;
        public float MaxLoopDelay;
        
        [HideInInspector] public bool IsPlaying;
        public bool changeVolume;
        public float ClipVolume;
        //[HideInInspector] public bool ShowExtras;
        //[HideInInspector] public bool HasEvents;

        //[HideInInspector] public bool IsTriggerEventEnter;
        //[HideInInspector] public bool IsTriggerEventStay;
        //[HideInInspector] public bool IsTriggerEventExit;
        //[HideInInspector] public bool IsCollisionEventEnter;
        //[HideInInspector] public bool IsCollisionEventStay;
        //[HideInInspector] public bool IsCollisionEventExit;

        public bool Has3DEffects;
        public float DopplerLevel3D;
        [HideInInspector]public float Spread3D;
        public float MaxVolume3D;
        public float MaxDistance3D;
        public float ReverbZoneMix;

        public bool BypassEffect;
        public bool BypassListenerEffect;
        public bool BypassReverbZone;
        public float StereoPan;

        public bool IsAmbientEvent;
        public UnityEvent ItemEvent;
        public UnityEvent TriggerEventEnter;
        public UnityEvent TriggerEventStay;
        public UnityEvent TriggerEventExit;
        public UnityEvent CollisionEventEnter;
        public UnityEvent CollisionEventStay;
        public UnityEvent CollisionEventExit;
        public UnityEvent AmbientEvent;
              
        private void Awake()
        {
            if (Source == null)
            {
                var sourcePrefab = Resources.Load<AudioSource>("Audio/AudioSource");
                Source = sourcePrefab;
                Source.gameObject.transform.position = gameObject.transform.position;               
            }

            if (HasStartDelay)
            {
                PlayOnStartDelay();
            }
        }

        private void Start()
        {
            if (IsAmbientEvent)
                OnAmbientEvent();

            if (IsPlayedOnStart)
                Play();
    Source.loop = IsLoop;
     
        }

        public virtual void Play()
        { }

        //Wrapper for play on start co-routine event
        public virtual void PlayOnStartDelay()
        {
            StartCoroutine(Delay(Random.Range(MinStartDelay, MaxStartDelay)));
        }

        protected IEnumerator Delay(float amt)
        {
            yield return new WaitForSeconds(amt);
            Play();
        }

        public virtual void OnSFXEvent()
        {
            ItemEvent.Invoke();
        }

        public virtual void OnAmbientEvent()
        {
            AmbientEvent.Invoke();
        }

        public virtual void OnTriggerEvent()
        {
            TriggerEventEnter.Invoke();
        }

        public void OnCollisionEvent()
        {
            CollisionEventEnter.Invoke();
        }

        public void OnTriggerEnter(Collider other)
        {
            TriggerEventEnter.Invoke();
        }

        public void OnTriggerExit(Collider other)
        {
            TriggerEventEnter.Invoke();
        }

        //WILL RAPPIDLY REPEAT UNTIL LEAVE ONTRIGGERSTAY
        public void OnTriggerStay(Collider other)
        {
            TriggerEventEnter.Invoke();
        }

        public void OnCollisionEnter(Collision collision)
        {
            CollisionEventEnter.Invoke();
        }

        //WILL RAPPIDLY REPEAT UNTIL LEAVE ONTRIGGERSTAY
        public void OnCollisionStay(Collision collision)
        {
            CollisionEventStay.Invoke();
        }

        public void OnCollisionExit(Collision collision)
        {
            CollisionEventExit.Invoke();
        }
    }
}