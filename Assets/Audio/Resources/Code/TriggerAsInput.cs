﻿using UnityEngine;
using UnityEngine.Events;

namespace Audio
{
    public class TriggerAsInput : MonoBehaviour
    {
        public UnityEvent MouseTriggerEvent;

        float endTime = 1;

        private void Update()
        {
            endTime -= Time.deltaTime;
        }

        private void OnMouseOver()
        {
            if (endTime <= 0)
            {
                MouseTriggerEvent.Invoke();
                endTime = 1;
                return;               
            }
            
        }
    }
}
