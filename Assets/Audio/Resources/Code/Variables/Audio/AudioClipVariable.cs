﻿
using UnityEngine;

namespace Audio
{
    [CreateAssetMenu(menuName = "Audio/Audio Clip", fileName = "_AudioClip")]
    public class AudioClipVariable : AudioEvent
    {
#if UNITY_EDITOR
        [Multiline]
        [HideInInspector] public string DeveloperDescription = "";
#endif

        [HideInInspector] public AudioClip Value;

        [HideInInspector] public bool IsMixed;

        public bool BypassEffects;
        public bool BypassListenerEffects;
        public bool BypassReverbZone;

        [HideInInspector] public bool IsLooping;

        //public RangedFloatVariable LoopTime;
        [MinMaxRange(.25f, 5f)] public RangedFloatVariable VolumeClip;
        [MinMaxRange(.25f, 3f)] public RangedFloatVariable Pitch;
        [MinMaxRange(-1f, 1f)] public RangedFloatVariable SteroPan;
        [MinMaxRange(0f, 1f)] public RangedFloatVariable SpatialBlend;
        [MinMaxRange(0f, 1.1f)] public RangedFloatVariable ReverbZoneMix;
        [MinMaxRange(0f, 5f)] public RangedFloatVariable Doppler;
        [MinMaxRange(0, 360)] public RangedFloatVariable Spread;
        [MinMaxRange(1, 500)] public RangedFloatVariable VolumeRolloff;

        public void SetClip(AudioClip clip)
        {
            Value = clip;
        }

        public void SetClip(MixedAudioClipVariable clip)
        {
            Value = clip.Value;
        }

        public void Looping(AudioSource source)
        {
            if (IsLooping)
            {
                source.loop = true;
                return;
            }
            if (!IsLooping)
            {
                source.loop = false;
            }
        }

        public void Mixed()
        {

        }

        public void Play(AudioSource source)
        {
            if (!IsMixed)
            {
                source.volume = 1;
                source.pitch = 1;

                source.Play();
                return;
            }
            else if (IsMixed)
            {
                source.clip = Value;

                source.bypassEffects = BypassEffects;
                source.bypassListenerEffects = BypassListenerEffects;
                source.bypassReverbZones = BypassReverbZone;

                source.volume = Random.Range(VolumeClip.minValue, VolumeClip.maxValue);
                source.pitch = Random.Range(Pitch.minValue, Pitch.maxValue);
                source.panStereo = Random.Range(SteroPan.minValue, SteroPan.maxValue);
                source.spatialBlend = Random.Range(SpatialBlend.minValue, SpatialBlend.maxValue);
                source.reverbZoneMix = Random.Range(ReverbZoneMix.minValue, ReverbZoneMix.maxValue);
                source.dopplerLevel = Random.Range(Doppler.minValue, Doppler.maxValue);
                source.spread = Random.Range(Spread.minValue, Spread.maxValue);

                source.Play();
            }
        }
    }
}