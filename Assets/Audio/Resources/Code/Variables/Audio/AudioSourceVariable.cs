﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Audio
{
    //Shutting off menu creation to reduce designer confusion.
    //Now auto adds itself where needed.


    //[CreateAssetMenu(menuName = "Audio/Audio Variable/Audio Source", fileName = "_AudioSourceVar")]
    public class AudioSourceVariable : ScriptableObject
    {
#if UNITY_EDITOR
        [Multiline]
        public string DeveloperDescription = "";
#endif

        public AudioSource Value;

        public void SetSource(AudioSource value)
        {
            Value = value;
        }

        public void SetSource(AudioSourceVariable value)
        {
            Value = value.Value;
        }
    }
}