﻿//From Unite Austin 2017 - Game Architecture with Scriptable Objects
    //https://youtu.be/raQ3iHhE_Kk
//https://github.com/roboryantron/Unite2017/blob/master/Assets/Code/Events/GameEvent.cs

using System.Collections.Generic;
using UnityEngine;

namespace Audio.Events
{
    [CreateAssetMenu]
    public class GameEvent : ScriptableObject
    {
        private readonly List<GameEventListner> eventListners = new List<GameEventListner>();

        public void Raise()
        {
            for (int i = eventListners.Count -1; i >= 0; i--)
            {
                eventListners[i].OnEventRaised();
            }
        }

        public void RegisterListener(GameEventListner listener)
        {
            if (!eventListners.Contains(listener))
            {
                eventListners.Add(listener);
            }
        }

        public void UnregisterListener(GameEventListner listener)
        {
            if (eventListners.Contains(listener))
            {
                eventListners.Remove(listener);
            }
        }
    }
}