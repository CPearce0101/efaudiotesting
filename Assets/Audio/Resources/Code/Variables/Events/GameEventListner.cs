﻿//From Unite Austin 2017 - Game Architecture with Scriptable Objects
    //https://youtu.be/raQ3iHhE_Kk
//https://github.com/roboryantron/Unite2017/blob/master/Assets/Code/Events/GameEventListener.cs

using UnityEngine;
using UnityEngine.Events;

namespace Audio.Events
{
    public class GameEventListner : MonoBehaviour
    {
        [Tooltip("Event to register with")]
        public GameEvent Event;

        [Tooltip("Responce object")]
        public UnityEvent Response;

        private void OnEnable()
        {
            Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            Event.UnregisterListener(this);
        }

        public void OnEventRaised()
        {
            Response.Invoke();
        }
    }
}
