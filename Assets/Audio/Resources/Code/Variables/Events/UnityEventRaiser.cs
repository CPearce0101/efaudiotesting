﻿//From Unite Austin 2017 - Game Architecture with Scriptable Objects
    //https://youtu.be/raQ3iHhE_Kk
//https://github.com/roboryantron/Unite2017/blob/master/Assets/Code/Variables/UnityEventRaiser.cs


using UnityEngine;
using UnityEngine.Events;

public class UnityEventRaiser : MonoBehaviour
{
    public UnityEvent OnEnableEvent;

    public void OnEnable()
    {
        OnEnableEvent.Invoke();
    }
}
