﻿// From Richard Fine's Unity Scriptable Object Talk: https://youtu.be/6vmRwLYWNRo
// https://bitbucket.org/richardfine/scriptableobjectdemo/src/default/Assets/ScriptableObject/Audio/MinMaxRangeAttribute.cs

using System;
using UnityEngine;

namespace Audio
{
    public class MinMaxRangeAttribute : Attribute
    {
        public float Min { get; private set; }
        public float Max { get; private set; }

        public MinMaxRangeAttribute(float min, float max)
        {
            Min = min;
            Max = max;
        }
    }
}