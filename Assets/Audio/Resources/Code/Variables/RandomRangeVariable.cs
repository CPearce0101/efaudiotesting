﻿using UnityEngine;

namespace Audio
{
    [CreateAssetMenu(menuName = "RandomRange", fileName = "_RandomRangeVar")]
    public class RandomRangeVariable : ScriptableObject
    {
#if UNITY_EDITOR
        [Multiline]
        public string DeveloperDescription = "";
#endif

        public float MinValue;
        public float MaxValue;

        public float Value;

        private void RandomValue()
        {
            Value = Random.Range(MinValue, MaxValue);
        }

        public void SetValue(float min, float max)
        {
            MinValue = min;
            MaxValue = max;

            RandomValue();
        }

        public void SetValue(int min, int max)
        {
            MinValue = min;
            MaxValue = max;

            RandomValue();
        }

        public void SetValue(RandomRangeVariable min, RandomRangeVariable max)
        {
            MinValue = min.MinValue;
            MaxValue = max.MaxValue;

            RandomValue();
        }

        public void ApplyMinChange(float amount)
        {
            MinValue += amount;

            RandomValue();
        }

        public void ApplyMaxChange(float amount)
        {
            MaxValue += amount;

            RandomValue();
        }

        public void ApplyMinChange(int amount)
        {
            MinValue += amount;

            RandomValue();
        }

        public void ApplyMaxChange(int amount)
        {
            MaxValue += amount;

            RandomValue();
        }

        public void ApplyMaxChange(RandomRangeVariable amount)
        {
            MaxValue = amount.MaxValue;

            RandomValue();
        }

        public void ApplyMinChange(RandomRangeVariable amount)
        {
            MinValue = amount.MinValue;

            RandomValue();
        }

    }
}