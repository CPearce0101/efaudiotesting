﻿// From Richard Fine's Unity Scriptable Object Talk: https://youtu.be/6vmRwLYWNRo
// https://bitbucket.org/richardfine/scriptableobjectdemo/src/default/Assets/ScriptableObject/Audio/RangedFloat.cs

namespace Audio
{

    [System.Serializable]
    public struct RangedFloatVariable
    {
        public float minValue;
        public float maxValue;
    }
}