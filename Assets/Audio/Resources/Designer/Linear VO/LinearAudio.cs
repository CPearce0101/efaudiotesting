﻿namespace Audio
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class LinearAudio : MonoBehaviour
    {
        [SerializeField] private List<AudioClip> clips;

        private List<AudioClip> currentClips;

        protected AudioSource source;

        public float maxVolumeDistance;
        public float maxDistance;

        private float savedVolume;

        private bool isPlaying;
        public bool IsNonConsecutive;

        public List<AudioClip> Clips { get { return clips; } set { clips = value; } }

        private void OnEnable()
        {
            source = gameObject.AddComponent<AudioSource>();

            source.minDistance = maxVolumeDistance;
            source.maxDistance = maxDistance;
            source.spatialBlend = 1;
            savedVolume = source.volume;

            currentClips = clips;
        }

        private void OnDisable()
        {
            if (source != null)
            {
                Destroy(source);
            }
        }

        //UPDATE WILL NOT REPLAY
        //NEEDS CLASS REDESIGN ADD LIST IN REVERSE, REMOVE FROM LIST CHECK WHEN 0 LEFT IN LIST, ISPLAY = TRUE, RESET LIST
        private IEnumerator Play(int order)
        {
            source.clip = clips[order];

            source.Play();

            if (clips.Contains(clips[order + 1]))
            {
                yield return new WaitForSeconds(source.clip.length + Random.Range(1f, 1.5f));

                StartCoroutine(Play(order + 1));
            }
        }

        private IEnumerator OnTriggerEnter(Collider other)
        {
            yield return new WaitForSeconds(Random.Range(.5f, 1f));

            if (!isPlaying)
            {
                StartCoroutine(Play(0));
                isPlaying = true;
            }
            if (isPlaying)
            {
                source.volume = savedVolume;
            }
        }

       private IEnumerator OnTriggerExit(Collider other)
        {
         source.volume = 0.5f - Time.deltaTime;
            yield return new WaitForSeconds(Random.Range(4f, 6f));
            source.volume=0;
            Debug.Log("Left Trigger " + this.name + "  volume: " + source.volume);
        }

    }
}