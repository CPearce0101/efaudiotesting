﻿namespace Audio
{
    using System.Collections.Generic;
    using UnityEngine;

    public class MultipleClips : SimpleAudioAction
    {
        public List<AudioClip> Clips = new List<AudioClip>();
     
        public override void Play()
        {
            AudioSource source = Instantiate(Source, gameObject.transform.position, gameObject.transform.rotation);
            source.pitch = 1;
           
            if (Has3DEffects)
            {
                source.dopplerLevel = DopplerLevel3D;
                source.spread = Spread3D;
                source.maxDistance = MaxDistance3D;
                source.spatialBlend = 1;
                source.minDistance = MaxVolume3D;
            }

            if (!changeVolume && ClipVolume == 0)
            {
                ClipVolume = 1;
            }
            if (!Has3DEffects && MaxDistance3D == 0)
            {
                MaxDistance3D = 1;
            }

            Source.bypassReverbZones = BypassReverbZone;
            Source.bypassListenerEffects = BypassListenerEffect;
            Source.bypassEffects = BypassEffect;
            Source.panStereo = StereoPan;
            Source.reverbZoneMix = ReverbZoneMix;

            if (changeVolume)
            {
                source.volume = ClipVolume;
            }

            var newClip = Clips[Random.Range(0, Clips.Count)];
            source.clip = newClip;

            if (IsLoop)
                source.loop = true;

            source.Play();

            if (IsCustomLooping)
            {
                Destroy(source.gameObject, source.clip.length);
                IsPlaying = false;
                Invoke("Play", source.clip.length + Random.Range(MinLoopDelay, MaxLoopDelay));
            }
            if (!IsCustomLooping && !IsLoop)
            {
                Destroy(source.gameObject, source.clip.length);
            }

            IsPlaying = false;
        }
               
        public void Play(int order)
        {
            AudioSource source = Instantiate(Source);

            source.clip = Clips[order - 1];

            //if (IsLoop)
            //    source.loop = true;

            source.Play();

            //if (!IsLoop)
            //    Destroy(source.gameObject, source.clip.length);

            if (IsCustomLooping)
            {
                Destroy(source.gameObject, source.clip.length);
                Invoke("Play", source.clip.length + MinLoopDelay);
            }
        }
        
        private void SetClip(AudioSource source)
        {
            source.clip = Clips[Random.Range(0, Clips.Count)];
        }

        //private void LoopClip(AudioSource source)
        //{
        //    source.loop = IsLoop;
        //}


    }
}