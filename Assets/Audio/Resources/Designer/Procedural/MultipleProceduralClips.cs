﻿/// <summary>
/// NEED TO UPDATE WITHOUT EFFECTING EDITOR
/// </summary>

namespace Audio
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class MultipleProceduralClips : AudioAction
    {
        public List<AudioClipVariable> Clips = new List<AudioClipVariable>();

        public override void Play()
        {
            AudioSource source = Instantiate(Source);

            var newClip = Clips[Random.Range(0, Clips.Count)];
            source.clip = newClip.Value;

            if (IsMixed)
            {
                newClip.IsMixed = IsMixed;
                MixClip(source);
            }

            if (IsLoop && !IsMixed)
            {
                source.loop = true;
            }

            source.Play();

            if (!IsLoop || IsLoop && IsMixed)
                Destroy(source.gameObject, source.clip.length);

            if (IsLoop && IsMixed)
            {
                Invoke("Play", source.clip.length + 0.01f);
            }
        }

        //Loop Delay
        public void Play(float delay)
        {
            AudioSource source = Instantiate(Source);

            var newClip = Clips[Random.Range(0, Clips.Count)];
            source.clip = newClip.Value;

            if (IsMixed)
            {
                newClip.IsMixed = IsMixed;
                MixClip(source);
            }

            if (IsLoop && !IsMixed)
            {
                source.loop = true;
            }

            source.Play();

            if (!IsLoop || IsLoop && IsMixed)
                Destroy(source.gameObject, source.clip.length);

            if (IsLoop && IsMixed)
            {
                Invoke("Play", delay);
            }
        }

        public void Play(int order)
        {
            AudioSource source = Instantiate(Source);

            source.clip = Clips[order - 1].Value;

            if (IsMixed && !IsLoop)
            {
                Clips[order].IsMixed = IsMixed;
                MixClip(source);
            }

            if (IsLoop && !IsMixed)
            {
                source.loop = true;
            }

            source.Play();

            if (!IsLoop || IsLoop && IsMixed)
                Destroy(source.gameObject, source.clip.length);

            if (IsLoop && IsMixed)
            {
                //Not mixing clip
                StartCoroutine(CustomLoop(source, order));
            }
        }

        //With looping delay
        //Not currently working within event
        public void PlaySpecific(float delay, int order)
        {
            AudioSource source = Instantiate(Source);

            source.clip = Clips[order].Value;

            if (IsMixed && !IsLoop)
            {
                Clips[order].IsMixed = IsMixed;
                MixClip(source);
            }

            if (IsLoop && !IsMixed)
            {
                source.loop = true;
            }

            source.Play();

            if (!IsLoop || IsLoop && IsMixed)
                Destroy(source.gameObject, source.clip.length);

            if (IsLoop && IsMixed)
            {
                //Not mixing clip
                StartCoroutine(CustomLoop(source, order, delay));
            }
        }

        private void SetClip(AudioSource source)
        {
            source.clip = Clips[Random.Range(0, Clips.Count)].Value;
        }

        IEnumerator CustomLoop(AudioSource source, int order)
        {
            if (IsMixed)
            {
                MixClip(source);
            }

            yield return new WaitForSeconds(source.clip.length);
            Play(order);
        }

        IEnumerator CustomLoop(AudioSource source, int order, float delay)
        {
            if (IsMixed)
            {
                MixClip(source);
            }

            yield return new WaitForSeconds(delay);
            Play(order);
        }

        private void MixClip(AudioSource source)
        {
            for (int i = 0; i < Clips.Count; i++)
            {
                source.bypassEffects = Clips[i].BypassEffects;
                source.bypassListenerEffects = Clips[i].BypassListenerEffects;
                source.bypassReverbZones = Clips[i].BypassReverbZone;

                source.volume = Random.Range(Clips[i].VolumeClip.minValue, Clips[i].VolumeClip.maxValue);
                source.pitch = Random.Range(Clips[i].Pitch.minValue, Clips[i].Pitch.maxValue);
                source.panStereo = Random.Range(Clips[i].SteroPan.minValue, Clips[i].SteroPan.maxValue);
                source.spatialBlend = Random.Range(Clips[i].SpatialBlend.minValue, Clips[i].SpatialBlend.maxValue);
                source.reverbZoneMix = Random.Range(Clips[i].ReverbZoneMix.minValue, Clips[i].ReverbZoneMix.maxValue);
                source.dopplerLevel = Random.Range(Clips[i].Doppler.minValue, Clips[i].Doppler.maxValue);
                source.spread = Random.Range(Clips[i].Spread.minValue, Clips[i].Spread.maxValue);
            }
        }
    }    
}

//THIS WILL PICK 1 RANDOM CLIP FROM LIST AND 
//REPEAT IT IF LOOP/MIXED LOOP IS SELECTED

    //for (int i = 0; i<Clips.Count; i++)
 //           {
 //               source.clip = Clips[i].Value;

 //               if (IsMixed)
 //               {
 //                   Clips[i].IsMixed = IsMixed;
 //                   MixClip(source);
 //               }

 //               if (IsLooping && !IsMixed)
 //               {
 //                   source.loop = true;
 //               }

 //               source.Play();

 //               if (!IsLooping || IsLooping && IsMixed)
 //                   Destroy(source.gameObject, source.clip.length);                               
 //           }