﻿namespace Audio
{
    using System.Collections;
    using UnityEngine;

    public class SingleProceduralClip : AudioAction
    {
        public AudioClipVariable Clip;

        public override void Play()
        {
            AudioSource source = Instantiate(Source);

            SetClip(source);

            if (IsMixed)
            {
                Clip.IsMixed = IsMixed;
                MixClip(source);
            }

            if (IsLoop && !IsMixed)
            {
                LoopClip(source);
            }

            source.Play();

            if (!IsLoop || IsLoop && IsMixed)
                Destroy(source.gameObject, source.clip.length);

            if (IsCustomLoop)
            {
                Destroy(source.gameObject, source.clip.length);
                Invoke("Play", source.clip.length + LoopDelay);
            }
        }

        public void Play(float delay)
        {
            AudioSource source = Instantiate(Source);

            SetClip(source);

            if (IsMixed)
            {
                Clip.IsMixed = IsMixed;
                MixClip(source);
            }

            if (IsLoop && !IsMixed)
            {
                source.loop = true;
            }

            source.Play();

            if (!IsLoop || IsLoop && IsMixed)
                Destroy(source.gameObject, source.clip.length);

            if (IsLoop && IsMixed)
            {
                StartCoroutine(CustomLoop(source, delay));
            }

            if (IsCustomLoop)
            {
                Destroy(source.gameObject, source.clip.length);
                Invoke("Play", source.clip.length + LoopDelay);
            }
        }

        private void SetClip(AudioSource source)
        {
            source.clip = Clip.Value;
        }

        private void LoopClip(AudioSource source)
        {
            source.loop = Clip.IsLooping;
        }

        private void MixedLoop(AudioSource source)
        {
            source.loop = false;
            Destroy(source.gameObject, source.clip.length);
            Invoke("Play", source.clip.length);
        }

        IEnumerator CustomLoop(AudioSource source, float delay)
        {
            if (IsMixed)
            {
                MixClip(source);
            }

            yield return new WaitForSeconds(source.clip.length + delay);
            Play(delay);
        }

        private void MixClip(AudioSource source)
        {
            source.bypassEffects = Clip.BypassEffects;
            source.bypassListenerEffects = Clip.BypassListenerEffects;
            source.bypassReverbZones = Clip.BypassReverbZone;

            source.volume = Random.Range(Clip.VolumeClip.minValue, Clip.VolumeClip.maxValue);
            source.pitch = Random.Range(Clip.Pitch.minValue, Clip.Pitch.maxValue);
            source.panStereo = Random.Range(Clip.SteroPan.minValue, Clip.SteroPan.maxValue);
            source.spatialBlend = Random.Range(Clip.SpatialBlend.minValue, Clip.SpatialBlend.maxValue);
            source.reverbZoneMix = Random.Range(Clip.ReverbZoneMix.minValue, Clip.ReverbZoneMix.maxValue);
            source.dopplerLevel = Random.Range(Clip.Doppler.minValue, Clip.Doppler.maxValue);
            source.spread = Random.Range(Clip.Spread.minValue, Clip.Spread.maxValue);
        }
    }
}
