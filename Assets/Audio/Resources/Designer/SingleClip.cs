﻿namespace Audio
{
    using System.Collections;
    using UnityEngine;

    public class SingleClip : SimpleAudioAction
    {
        public AudioClip Clip;

        public override void Play()
        {
            if (!IsPlaying)
            {
                AudioSource source = Instantiate(Source, gameObject.transform.position, gameObject.transform.rotation);
                source.pitch = 1; //If not set caused bug with audio

                IsPlaying = true;
                
                if (Has3DEffects)
                {
                    source.dopplerLevel = DopplerLevel3D;
                    source.spread = Spread3D;
                    source.maxDistance = MaxDistance3D;
                    source.spatialBlend = 1;
                }

                if (!changeVolume && ClipVolume == 0)
                {
                    ClipVolume = 1;
                }
                if (!Has3DEffects && MaxDistance3D == 0)
                {
                    MaxDistance3D = 1;
                }

                source.bypassReverbZones = BypassReverbZone;
                source.bypassListenerEffects = BypassListenerEffect;
                source.bypassEffects = BypassEffect;
                source.panStereo = StereoPan;
                source.reverbZoneMix = ReverbZoneMix;

                if (changeVolume)
                {
                    source.volume = ClipVolume;
                }

                SetClip(source);

                //if (IsLoop)
                //{
                //    LoopClip(source);
                //}

                source.Play();
                
                if (IsCustomLooping)
                {
                    Destroy(source.gameObject, source.clip.length);
                    Invoke("Play", source.clip.length + Random.Range(MinLoopDelay, MaxLoopDelay));
                }
                else if (!IsCustomLooping)
                {
                    Destroy(source.gameObject, source.clip.length);
                }

                IsPlaying = false;
            }            
        }    

        //wrapper for delay co-routine
        public void PlayDelayed(float amt)
        {
            StartCoroutine(Delay(amt));
        }                    

        private void SetClip(AudioSource source)
        {
            source.clip = Clip;
        }

        //private void LoopClip(AudioSource source)
        //{
        //    source.loop = IsLoop;
        //}       
    }
}